Warhammerk 40k Battle Reports
==

This project contains battle reports from games of Warhammer 40k with friends,
as well as a stylesheet and supporting images for creating new battle reports.

The battle report description text is © Brent Houghton, no rights reserved. The
page design and layout is © Brent Houghton and may be reused under the terms of
the [AGPL license](http://www.gnu.org/licenses/agpl-3.0-standalone.html).

This project also uses background images from
[subtlepatterns.com](http://subtlepatterns.com), which can be found in the
lib/ folder.

